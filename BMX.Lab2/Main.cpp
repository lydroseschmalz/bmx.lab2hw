// Creating Card Game
#include <iostream>
#include <conio.h>
#include <string>

using namespace std;


enum Suit
{
	HEARTS,
	DIAMONDS,
	SPADES,
	CLUBS
};
enum Rank
{
	ACE = 14,
	KING = 13,
	QUEEN = 12,
	JACK = 11,
	TEN = 10,
	NINE = 9,
	EIGHT = 8,
	SEVEN = 7,
	SIX = 6,
	FIVE = 5,
	FOUR = 4,
	THREE = 3,
	TWO = 2
};

struct Card
{
	Rank rank;
	Suit suit;
};

void PrintCard(Card &card)
{
	cout << "The ";
	switch (card.rank) 
	{
	case 2: cout << "Two"; break;
	case 3: cout << "Three"; break;
	case 4: cout << "Four"; break;
	case 5: cout << "Five"; break;
	case 6: cout << "Six"; break;
	case 7: cout << "Seven"; break;
	case 8: cout << "Eight"; break;
	case 9: cout << "Nine"; break;
	case 10: cout << "Ten"; break;
	case 11: cout << "Jack"; break;
	case 12: cout << "Queen"; break;
	case 13: cout << "King"; break;
	case 14: cout << "Ace"; break;
	}

	cout << " of ";
	switch (card.suit) 
	{
	case 0: cout << "Hearts"; break;
	case 1: cout << "Diamonds"; break;
	case 2: cout << "Spades"; break;
	case 3: cout << "Clubs"; break;
	}

	cout << "\n";
}

Card HighCard(Card &card1, Card &card2) 
{
	if (card1.rank > card2.rank)
	{
		return card1;
	}
	if (card2.rank > card1.rank)
	{
		return card2;
	}
	card1;
}

int	main()
{
	Card card1;
	card1.suit = HEARTS;
	card1.rank = TEN;

	PrintCard(card1);

	

	_getch();
	return 0;
}